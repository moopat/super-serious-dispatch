package at.moop.dispatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("spring.xml")
@ComponentScan
public class SuperSeriousDispatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuperSeriousDispatchApplication.class, args);
	}
}
